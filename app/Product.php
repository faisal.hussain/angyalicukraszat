<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /*
    In addition, Eloquent assumes that the primary key is an incrementing integer value,
    which means that by default the primary key will automatically be cast to an int.
    If you wish to use a non-incrementing or a non-numeric primary key you must
    set the public $incrementing property on your model to false
    */
    protected $keyType = 'string';
    public $incrementing = false;
    protected $primaryKey = 'name';

    protected $casts = [
        'slice_numbers' => 'collection',
    ];
}
