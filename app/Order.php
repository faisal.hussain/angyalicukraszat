<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $dates = ['order_date', 'updated_at'];
    protected $casts = [
        'normal_orders' => 'array'
    ];

    public function customer()
    {
        return $this->belongsTo(User::class, 'customer_id');
    }

    public function slices()
    {
        return $this->hasMany(Slice::class, 'order_id');
    }

    // https://medium.com/@petehouston/laravel-fact-make-computed-attributes-for-eloquent-models-fc78fe5f1aa4
    public function getEstimatedPriceAttribute()
    {
        $price = 0.00;

        foreach (Slice::where('order_id', $this->id)->get() as $key => $value) {
            $price += (double)$value['price'] * (double)$value['quantity'];
        }
        
        return $price;
    }

    // https://medium.com/@petehouston/laravel-fact-make-computed-attributes-for-eloquent-models-fc78fe5f1aa4
    public function getSlicesCountAttribute()
    {
        $count = 0;

        foreach (Slice::where('order_id', $this->id)->get() as $key => $value) {
            $count += (int)$value['quantity'];
        }
        
        return $count;
    }
}
