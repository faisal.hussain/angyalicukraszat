<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SpecialOrder extends Model
{
    public function customer()
    {
        return $this->belongsTo(User::class, 'customer_id');
    }

    public function product()
    {
        return $this->hasOne(Product::class, 'product_id');
    }
}
