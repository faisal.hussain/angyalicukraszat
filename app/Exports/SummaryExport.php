<?php

namespace App\Exports;

use App\Order;
use App\Product;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Excel;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;

class SummaryExport implements WithEvents
{

    public function registerEvents(): array
    {
        return [
            BeforeExport::class => function (BeforeExport $spreadsheet) {
                $spreadsheet->writer->reopen(new \Maatwebsite\Excel\Files\LocalTemporaryFile(storage_path('osszesito.xlsx')), Excel::XLSX);
                $writer = $spreadsheet->getWriter()->getSheetByIndex(0);

                $enabledProducts = Product::all();

                // get() will give us a collection instead of a Builder instance
                $todaysOrders = Order::whereDay('order_date', '=', date('d'))->get()->sortBy(function ($order, $key) {
                    return $order->customer->summary_ranking;
                })->values()->all();
                $orderCount = count($todaysOrders);

                // Check if we have enough rows
                $writer->insertNewRowBefore(4, count($enabledProducts) - 1);

                // Check if we have enough columns. We only add. No deletions
                if ($orderCount > 27) {
                    $writer->insertNewColumnBefore('AF', $orderCount - 28);
                }

                $rightOsszColumn = ExportUtilities::numToAlpha($orderCount + 3);
                $rightEgysegarColumn = ExportUtilities::numToAlpha($orderCount + 4);
                $rightOsszErtekColumn = ExportUtilities::numToAlpha($orderCount + 5);

                $currentRow = 0;
                foreach ($enabledProducts as $product_no => $details) {
                    $currentRow = ($product_no + 3);

                    $writer->setCellValue("A" . $currentRow, $details->name);
                    $writer->setCellValue("C" . $currentRow, "=SUM(D" . $currentRow . ":" . Coordinate::stringFromColumnIndex($orderCount + 3) . $currentRow . ")");
                    $writer->setCellValue($rightOsszColumn . $currentRow, "=SUM(D" . $currentRow . ":" . Coordinate::stringFromColumnIndex($orderCount + 3) . $currentRow . ")");

                    // Summary will always contain prices at the time of creation
                    $writer->setCellValue($rightEgysegarColumn . $currentRow, $details->price);
                    $writer->setCellValue($rightOsszErtekColumn . $currentRow, "=" . $rightOsszColumn . $currentRow . "*" . $rightEgysegarColumn  . $currentRow);
                }

                // Fill formulas for ÖSSZESEN row
                $writer->setCellValue("C" . ($currentRow + 1), "=SUM(C3:C" . $currentRow . ")");

                foreach ($todaysOrders as $key => $value) {
                    $currentColumn = ExportUtilities::numToAlpha($key + 3);

                    $writer->setCellValue($currentColumn . "2", $value->customer->name);

                    // Fill formulas for ÖSSZESEN row
                    $writer->setCellValue($currentColumn . ($currentRow + 1), "=SUM(" . $currentColumn . "3:" . $currentColumn . $currentRow . ")");

                    foreach ($value->slices as $slice_no => $details) {
                        $writer->setCellValue($currentColumn . "" . ($slice_no + 3), $details->quantity);
                    }
                }

                // Fill formulas for ÖSSZESEN row
                $writer->setCellValue(ExportUtilities::numToAlpha($orderCount + 3) . ($currentRow + 1), "=SUM(" . ExportUtilities::numToAlpha($orderCount + 3) . "3:" . ExportUtilities::numToAlpha($orderCount + 3) . $currentRow . ")");
                $writer->setCellValue(ExportUtilities::numToAlpha($orderCount + 5) . ($currentRow + 1), "=SUM(" . ExportUtilities::numToAlpha($orderCount + 5) . "3:" . ExportUtilities::numToAlpha($orderCount + 5) . $currentRow . ")");

                return $writer;
            },
        ];
    }
}
