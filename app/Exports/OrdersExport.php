<?php

namespace App\Exports;

use App\Order;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Excel;

class OrdersExport implements WithEvents
{
    public function __construct(int $id)
    {
        $this->order = Order::find($id);
    }

    /**
     * @return array
     */
    public function registerEvents(): array
    {
        return [
            BeforeExport::class => function (BeforeExport $spreadsheet) {
                $spreadsheet->writer->reopen(
                    new \Maatwebsite\Excel\Files\LocalTemporaryFile(storage_path('rendelolap.xlsx')),
                    Excel::XLSX
                );
                $writer = $spreadsheet->getWriter()->getSheetByIndex(0);

                $writer->setCellValue('B1', $this->order->customer->name);
                $writer->setCellValue('B2', Carbon::parse($this->order->order_date)->format('m/d/Y'));
                $writer->insertNewRowBefore(5, count($this->order->slices) - 1);

                foreach ($this->order->slices as $slice_no => $details) {
                    $writer->setCellValue("A" . ($slice_no + 4), $details->product->name);
                    $writer->setCellValue("B" . ($slice_no + 4), $details->quantity);
                    $writer->setCellValue("C" . ($slice_no + 4), $details->price);
                    $writer->setCellValue("D" . ($slice_no + 4), $details->quantity * $details->price);
                    $writer->setCellValue("E" . ($slice_no + 4), $details->product->slice_numbers);
                    $writer->setCellValue("F" . ($slice_no + 4), "4 nap");
                }

                $writer->setCellValue("B" . (count($this->order->slices) + 4), "=SUM(B4:B" . count($this->order->slices) . ")");
                $writer->setCellValue("D" . (count($this->order->slices) + 4), "=SUM(D4:D" . count($this->order->slices) . ")");

                return $writer;
            },
        ];
    }
}
