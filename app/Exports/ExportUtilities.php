<?php

namespace App\Exports;

class ExportUtilities
{
    public static function numToAlpha($n)
    {
        for ($r = ""; $n >= 0; $n = intval($n / 26) - 1) {
            $r = chr($n % 26 + 0x41) . $r;
        }
        return $r;
    }
}
