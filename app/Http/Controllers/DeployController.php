<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\Process\Process;

class DeployController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $process = new Process(['git', '--version']);
        $process->run();
        $gitversion = $process->getOutput();

        $process = new Process(['git', 'rev-parse', '--abbrev-ref', 'HEAD']);
        $process->run();
        $gitbranch = $process->getOutput();

        $process = new Process(['git', 'log', '-1']);
        $process->run();
        $gitcommit = $process->getOutput();

        $process = new Process(['git', 'status']);
        $process->run();
        $gitstatus = $process->getOutput();

        return view('deploy', [
            'gitversion' => $gitversion,
            'gitbranch' => $gitbranch, 'gitcommit' => $gitcommit,
            'gitstatus' => $gitstatus
        ]);
    }

    /**
     *
     *
     * @return \Illuminate\Http\Response
     */
    public function execute(Request $request)
    {
        if ($request->user()->isAdmin == 1) {
            $process = Process::fromShellCommandline($request->command);
            $process->run();
            return response()->json(['output' => $process->getOutput()]);
        } else {
            abort(401);
        }
    }
}
