<?php

namespace App\Http\Controllers;

use App\SpecialOrder;
use App\Product;
use App\User;
use App\Slice;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use App\Exports\OrdersExport;
use Maatwebsite\Excel\Facades\Excel;

class SpecialOrdersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Gate::allows('is-Admin')) {
            return view('specialorders.index')->with('specialorders', SpecialOrder::all());
        } else {
            return view('specialorders.index')->with('specialorders', Auth::user()->specialorders);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('specialorders.create')->with('products', Product::all())->with('customers', User::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'form_customer' => 'bail|required',
            'form_order_date' => 'required|date'
        ]);

        $order = new Order();
        $order->customer_id = User::find($request->form_customer)->id;
        $order->order_date = Carbon::createFromFormat('m/d/Y', $request->form_order_date);
        $order->special_orders = $request->form_special_product;
        $order->estimated_price = $request->form_price;
        $order->slices_count = $request->form_quantity;
        $order->created_at = Carbon::now();
        $order->updated_at = Carbon::now();

        $order->save();

        foreach ($request->form_normal_product as $key => $value) {
            $slice = new Slice();
            $slice->product_id = $key;
            $slice->order_id = $order->id;
            $slice->quantity = $value['quantity'];
            $slice->price = $value['price'];
            $slice->save();
        }

        return redirect('orders');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SpecialOrder  $specialOrder
     * @return \Illuminate\Http\Response
     */
    public function show(SpecialOrder $specialOrder)
    {
        return Excel::download(new OrdersExport($id), "Order" . $id . ".xlsx");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SpecialOrder  $specialOrder
     * @return \Illuminate\Http\Response
     */
    public function edit(SpecialOrder $specialOrder)
    {
        $order = Order::find($id);
        $slices = Slice::where('order_id', $id)->get();
        return view('orders.edit')->with('order', $order)->with('slices', $slices);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SpecialOrder  $specialOrder
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SpecialOrder $specialOrder)
    {
        $order = Order::find($id);
        $order->special_orders = $request->form_special_product;
        $order->estimated_price = $request->form_price;
        $order->slices_count = $request->form_quantity;
        $order->updated_at = Carbon::now();
        $order->save();

        foreach ($request->form_normal_product as $key => $value) {
            $slice = Slice::where([
                ['order_id', '=', $order->id],
                ['product_id', '=', $key],
            ])->first();
            $slice->quantity = $value['quantity'];
            $slice->save();
        }

        return redirect('orders');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SpecialOrder  $specialOrder
     * @return \Illuminate\Http\Response
     */
    public function destroy(SpecialOrder $specialOrder)
    {
        Product::destroy($name);

        return redirect()->back();
    }
}
