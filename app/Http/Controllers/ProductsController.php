<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;

class ProductsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('products.index')->with('products', Product::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('products.create')->with('products', Product::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'form_name' => 'bail|required|unique:products,name',
            'form_weight' => 'numeric',
            'form_energy' => 'numeric',
            'form_splice_numbers' => 'string',
            'form_price' => 'numeric'
        ]);

        $path = null;
        if ($request->hasFile('thumbnailFile')) {
            $allowedfileExtension = ['jpeg', 'jpg'];
            $thumbnail = $request->file('thumbnailFile');

            $extension = $thumbnail->getClientOriginalExtension();
            $check = in_array($extension, $allowedfileExtension);

            if ($check) {
                $path = $thumbnail->store('product_thumbnails');
            }
        }

        $product = new Product();
        $product->name = $request->form_name;
        $product->weight = $request->form_weight;
        $product->energy = $request->form_energy;
        $product->slice_numbers = $request->slices;
        $product->price = $request->form_price;
        $product->thumbnail = $path;
        $product->save();

        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::where('id', $id)->first();
        return view('products.edit')->with('product', $product);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'form_name' => 'bail|required',
            'form_weight' => 'numeric',
            'form_energy' => 'numeric',
            'form_splice_numbers' => 'string',
            'form_price' => 'numeric'
        ]);

        $product = Product::where('id', $id)->first();

        if ($request->hasFile('thumbnail')) {
            $allowedfileExtension = ['jpeg', 'jpg'];
            $thumbnail = $request->file('thumbnail');

            $extension = $thumbnail->getClientOriginalExtension();
            $check = in_array($extension, $allowedfileExtension);

            if ($check) {
                $path = $thumbnail->store('product_thumbnails');
                $product->thumbnail = $path;
            }
        }

        $product->weight = $request->form_weight;
        $product->energy = $request->form_energy;
        $product->slice_numbers = $request->slices;
        $product->price = $request->form_price;
        $product->save();

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string  $name
     * @return \Illuminate\Http\Response
     */
    public function destroy($name)
    {
        Product::destroy($name);

        return redirect()->back();
    }
}
