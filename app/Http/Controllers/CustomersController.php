<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class CustomersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('customers.index')->with('customers', User::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('customers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'bail|required',
            'username' => 'required',
            'email' => 'required',
            'password' => 'required'
        ]);

        $customer = new User();
        $customer->name = $request->name;
        $customer->username = $request->username;
        $customer->summary_ranking = $request->ranking;
        $customer->email = $request->email;
        $customer->password = Hash::make($request->password);
        if ($request->has('isAdminCheck')) {
            $customer->isAdmin = 1;
        } else {
            $customer->isAdmin = 0;
        }
        
        $customer->created_at = Carbon::now();
        $customer->updated_at = Carbon::now();

        $customer->save();

        return redirect('customers');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customer = User::find($id);
        return view('customers.edit')->with('customer', $customer);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'bail|required',
            'username' => 'required',
            'email' => 'required'
        ]);

        $currentid = Auth::id();
        $customer = User::find($id);
        $customer->name = $request->name;
        $customer->username = $request->username;
        $customer->summary_ranking = $request->ranking;
        $customer->email = $request->email;

        // Prevents current user from accidentally removing privilage
        if ($id != $currentid) {
            if ($request->has('isAdminCheck')) {
                $customer->isAdmin = 1;
            } else {
                $customer->isAdmin = 0;
            }
        }

        // Change the password if they typed in a new one
        if (!empty($request->input('password'))) {
            $customer->password = Hash::make($request->password);
        }
        
        $customer->updated_at = Carbon::now();
        $customer->save();

        return redirect('customers');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::destroy($id);

        return redirect('customers');
    }
}
