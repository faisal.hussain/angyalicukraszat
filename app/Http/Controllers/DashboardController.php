<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Order;
use Illuminate\Http\Request;
use App\Exports\SummaryExport;
use Maatwebsite\Excel\Facades\Excel;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $todaysOrders = Order::whereDay('order_date', '=', date('d'));
        $orderCount = $todaysOrders->count();
        $slicecount = 0;
        foreach ($todaysOrders->get() as $key => $value) {
            $slicecount += $value->slices_count;
        }
        return view('dashboard')->with('ordercount', $orderCount)->with('slicecount', $slicecount);
    }

    public function summary()
    {
        return Excel::download(new SummaryExport(), Carbon::now()->format('m-d-Y') . ".xlsx");
    }
}
