<?php

// current directory
echo getcwd() . "\n";

chdir('..');

// current directory
echo getcwd() . "\n";

try {
    // decompress from gz
    $p = new PharData('vendor.tar.gz');
    $p->decompress(); // creates vendor.tar

    // unarchive from the tar
    $phar = new PharData('vendor.tar');
    $phar->extractTo(getcwd(), null, true);

    unlink("vendor.tar");
    unlink("vendor.tar.gz");
    system("rm -rf vendordelete");
} catch (Exception $e) {
    echo $e->getMessage() . "\n";
    http_response_code(500);
}

http_response_code(200);