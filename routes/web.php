<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource('/products', 'ProductsController', ['except' => ['show']]);

Route::resource('/orders', 'OrdersController');
Route::resource('/specialorders', 'SpecialOrdersController');

Route::resource('/customers', 'CustomersController');

Route::get('deploy', 'DeployController@index')->name('deploy.index');
Route::post('deploy', 'DeployController@execute')->name('deploy.execute');

Route::get('/summary', 'DashboardController@summary')->name('dashboard.summary');
Route::any('/', array( 'as' => 'home', 'uses' => 'DashboardController' ));

Auth::routes();
