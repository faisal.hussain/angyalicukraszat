## About This Project

<table>
<tr>
<th> Previous Workflow </th>
<th> Improved Workflow </th>
</tr>
<tr>
<td>

```java
foreach Email in Inbox
{
    AddToSummary();
}
PrintSummary();
foreach Order in Summary
{
    CreateBill();
    PrintBill();
}
```

</td>
<td>

```java
DownloadSummary();
PrintSummary();
foreach Order in Summary
{
    DownloadBill();
    PrintBill();
}
```

</td>
</tr>
</table

Creating excel files is repititive and prone to introduction of errors if an
employee is not paying full attention.

The previous workflow also used up 2 hours of employee time.


This project aims to **reduce** errors while giving the same output in **less** time