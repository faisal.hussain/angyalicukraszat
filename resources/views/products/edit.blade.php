@extends('layouts.app')

@section('content-header')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Product - Edit</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Products</li>
                </ol>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
</section>
@endsection

@section('content')
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-12">
            <div class="card">

                <div class="card-header">
                    <h3 class="card-title">Edit Product</h3>

                    <div class="card-tools">
                        <!-- Buttons, labels, and many other things can be placed here! -->
                        <!-- Here is a label for example -->
                    </div>

                </div>

                <div class="card-body">

                    <form id="createProductForm" action="{{ route('products.update', $product->id)}}" method="POST" enctype="multipart/form-data">
                        @csrf

                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <div class="label">Name</div>
                                    <input type="text" class="form-control" name="form_name" id="name" value="{{$product->name}}" required>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <div class="label">Price (HUF)</div>
                                    <input type="number" class="form-control" name="form_price" id="price" step="0.01" min="0" value="{{$product->price}}" required>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-3">
                                <div class="form-group">
                                    <div class="label">Weight (g)</div>
                                    <input type="number" class="form-control" name="form_weight" id="weight" step="0.01" min="0" value="{{$product->weight}}" required>
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="form-group">
                                    <div class="label">Energy (kcal)</div>
                                    <input type="number" class="form-control" name="form_energy" id="energy" step="0.01" min="0" value="{{$product->energy}}" required>
                                </div>
                            </div>
                            <div class="col-6">
                                <label for="slices_number_label">
                                    Number of Slices:

                                    <select class="slices-multiple js-states form-control" name="slices[]" multiple="multiple" id="slices_number_label" style="width: 75%">
                                        @foreach ($product->slice_numbers as $slice)
                                        <option value={{$slice}} selected>{{$slice}}</option>
                                        @endforeach
                                    </select>
                                </label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">

                                <div class="form-group">
                                    <label for="thumbnailFile">Upload thumbnail to override</label>
                                    <div class="input-group">
                                        <!-- https://github.com/Johann-S/bs-custom-file-input -->
                                        <div class="custom-file">
                                            <label class="custom-file-label" for="thumbnailFile">Choose file</label>
                                            <input type="file" class="form-control" id="thumbnailFile" accept=".jpg,.jpeg">
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </form>
                </div>
                {{-- CardBody --}}

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary" form="createProductForm">Submit</button>
                </div>

            </div>
            {{-- card --}}

        </div>
    </div>
</section>

@endsection

@section('customjs')
<script src="{{ asset('AdminLTE/plugins/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>
<script>
    $(function() {

        //Initialize Select2 Elements
        $('.slices-multiple').select2({
            theme: "classic"
        });

        // https://github.com/Johann-S/bs-custom-file-input
        bsCustomFileInput.init()
    });
</script>
@endsection