@extends('layouts.app')

@section('customcss')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('AdminLTE/plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
@endsection

@section('content-header')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Products</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Products</li>
                </ol>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
</section>
@endsection

@section('content')
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-12">
            <div class="card">

                <div class="card-header">
                    <h3 class="card-title">DataTable</h3>

                    <div class="card-tools">
                        <!-- Buttons, labels, and many other things can be placed here! -->
                        <!-- Here is a label for example -->
                        <a href="{{route('products.create')}}" type="button" class="btn btn-tool">
                            <i class="fas fa-plus-circle"></i>
                        </a>
                    </div>

                </div>

                <div class="card-body">
                    <div id="productsDataTable" class="dataTables_wrapper dt-bootstrap4">
                        <div class="row">
                            <div class="col-sm-12 col-md-6"></div>
                            <div class="col-sm-12 col-md-6"></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <table class="table table-bordered table-hover dataTable table-sm" role="grid" id="productsTable">
                                    <thead>
                                        <tr>
                                            <th tabindex="0" rowspan="1" colspan="1"></th>
                                            <th class="sorting_asc" tabindex="0" rowspan="1" colspan="1">Name</th>
                                            <th class="sorting" tabindex="0" rowspan="1" colspan="1">Weight</th>
                                            <th class="sorting" tabindex="0" rowspan="1" colspan="1">Energy</th>
                                            <th class="sorting" tabindex="0" rowspan="1" colspan="1">Slice Numbers</th>
                                            <th class="sorting" tabindex="0" rowspan="1" colspan="1">Price</th>
                                            <th colspan="1">Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($products as $product)
                                        <tr role="row">
                                            <td><img src="storage/{{$product->thumbnail}}" width="40" height="30"></td>
                                            <td class="sorting_1">{{$product->name}}</td>
                                            <td>{{$product->weight}}</td>
                                            <td>{{$product->energy}}</td>
                                            <td>{{$product->slice_numbers->implode(',')}}</td>
                                            <td>{{$product->price}}</td>
                                            <td>
                                                <a href="{{ route('products.edit', $product->id) }}" class="btn btn-primary btn-sm">
                                                    <i class="fas fa-edit"></i>
                                                </a>

                                                <button class="btn btn-danger btn-sm" type="submit" form="deleteForm" formaction="{{ route('products.destroy', $product->name)}}"><i class="far fa-minus-square"></i></button>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    {{-- productsDataTable --}}

                </div>
                {{-- CardBody --}}

            </div>
            {{-- card --}}

        </div>
    </div>
</section>

@section('customjs')

<script src="{{ asset('AdminLTE/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src=" {{ asset('AdminLTE/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>

<script>
    $(document).ready(function() {
        $('#productsTable').DataTable({
            "columnDefs": [{
                "width": "5%",
                "targets": 0
            }],
            "order": [
                [1, "desc"]
            ]
        });
    });
</script>

@endsection

@endsection