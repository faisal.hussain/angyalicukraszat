@extends('layouts.app')

@section('customcss')
@endsection

@section('content-header')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Deployment</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Deployment</li>
                </ol>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
</section>
@endsection

@section('content')
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-6">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">GIT Info</h3>
                </div>
                <div class="card-body">

                    <strong><i class="fas fa-book mr-1"></i> Version</strong>
                    <p class="text-muted">{{$gitversion}}</p>
                    <hr>

                    <strong><i class="fas fa-map-marker-alt mr-1"></i> Current Branch</strong>
                    <p class="text-muted">{{$gitbranch}}</p>
                    <hr>

                    <strong><i class="fas fa-pencil-alt mr-1"></i> Current Commit</strong>
                    <p class="text-muted">{{$gitcommit}}</p>

                    <strong><i class="fas fa-pencil-alt mr-1"></i> GIT Status</strong>
                    <p class="text-muted">{{$gitstatus}}</p>
                </div>
            </div>
        </div>
        <div class="col-6">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Execute Commands</h3>
                </div>
                <div class="card-body">
                    <div class="input-group mb-0">
                        <input class="form-control" placeholder="composer update" id="command">
                        <div class="input-group-append">
                            <button class="btn btn-danger">Execute</button>
                        </div>
                    </div>
                    <br>
                    <textarea id="executionResponse" class="form-control" rows="3" placeholder="Enter ..."></textarea>
                    <br>
                    Useful Commands:
                    <br>
                    <code>git fetch --all</code>
                    <br>
                    <code>git reset --hard origin/master</code>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection

@section('customjs')

<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(".btn-danger").click(function(e){
        e.preventDefault();
        $.ajax({
            type:'POST',
            url:"{{ route('deploy.execute') }}",
            data:{command:document.getElementById('command').value},
            success:function(data)
            {
                document.getElementById('executionResponse').innerText = data.output;
            }
        });
        });
</script>

@endsection