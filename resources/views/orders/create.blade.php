@extends('layouts.app')

@section('content-header')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Order - Create</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Orders</li>
                </ol>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
</section>
@endsection

@section('content')
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-12">
            <div class="card">

                <div class="card-header">
                    <h3 class="card-title">New Order</h3>

                    <div class="card-tools">
                        <!-- Buttons, labels, and many other things can be placed here! -->
                        <!-- Here is a label for example -->
                    </div>

                </div>

                <div class="card-body">

                    <form id="createOrderForm" action="{{action('OrdersController@store')}}" method="POST">
                        @csrf

                        <div class="form-row align-items-center">

                            <div class="col-4">
                                @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif
                                <div class="form-group">
                                    <label for="inlineFormInput">Customer:</label>
                                    @can('is-Admin')
                                    <select class="form-control select2" style="width: 100%" name="form_customer"
                                        id="form_customer">
                                        @foreach ($customers as $customer)
                                        <option value={{$customer->id}}>{{$customer->name}}</option>
                                        @endforeach
                                        <option value={{ Auth::user()->id }} selected>{{ Auth::user()->name }}</option>
                                    </select>
                                    @else
                                    <input type="text" class="form-control mb-2" name="name"
                                        placeholder="{{ Auth::user()->name }}" disabled="disabled">
                                    <input type="hidden" class="form-control mb-2" name="form_customer"
                                        value="{{ Auth::user()->id }}">
                                    @endcan
                                </div>

                                <label>Date range:</label>

                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="far fa-calendar-alt"></i>
                                            </span>
                                        </div>
                                        <input type="text" class="form-control float-right" id="orderDate"
                                            name="form_order_date">
                                    </div>
                                    <!-- /.input group -->
                                </div>
                            </div>
                            {{-- This separates the items from above. Separator. Dont remove --}}
                            <div class="col-2">
                                <input type="hidden" name="form_price" id="form_price">
                                <input type="hidden" name="form_quantity" id="form_quantity">
                            </div>
                            <div class="col-4">
                                <div class="small-box bg-info">
                                    <div class="inner">
                                        <h3 id="pricetag">0 HUF (0 Slices)</h3>
                                        <p>Calculated Price</p>
                                    </div>
                                    <div class="icon">
                                        <i class="fas fa-shopping-cart"></i>
                                    </div>
                                    <a href="#" class="small-box-footer">
                                        More info <i class="fas fa-arrow-circle-right"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        @foreach ($products as $product)
                        <div class="col-6">
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">{{$product->name}}</div>
                                </div>
                                <input type="number" class="form-control"
                                    name="form_normal_product[{{$product->id}}][quantity]" value=0
                                    onchange="calcPrice()">
                                <input type="number" class="form-control"
                                    name="form_normal_product[{{$product->id}}][price]" value={{$product->price}}
                                    hidden>
                                <div class="input-group-append">
                                    <span class="input-group-text">{{$product->price}} HUF/db</span>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        <hr>
                        <h2>Special Orders:</h2>
                        <div class="mb-3">
                            <textarea class="textarea" placeholder="Place some text here" name="form_special_product"
                                style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                        </div>
                    </form>
                </div>
                {{-- CardBody --}}

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary" form="createOrderForm">Submit</button>
                </div>

            </div>
            {{-- card --}}

        </div>
    </div>
</section>

@endsection

@section('customjs')
<script>
    $(function(){
        //Date range picker with time picker
        $('#orderDate').daterangepicker({
        timePicker: false, //<==MAKE THE CHANGE HERE
        singleDatePicker: true, //<==MAKE THE CHANGE HERE
        });

        //Initialize Select2 Elements
        $('.select2').select2();

        // Summernote
        $('.textarea').summernote();

        calcPrice();
    });

    function calcPrice()
    {
        var total = 0;
        var quantity = 0;

        @foreach ($products as $product)
            total += parseInt(document.getElementsByName("form_normal_product[{{$product->id}}][quantity]")[0].value) * 
            document.getElementsByName("form_normal_product[{{$product->id}}][price]")[0].value;

            quantity += parseInt(document.getElementsByName("form_normal_product[{{$product->id}}][quantity]")[0].value);
        @endforeach
        
        document.getElementById("pricetag").innerText = total + " HUF (" + quantity + " slices)";
        document.getElementById("form_price").value = total;
        document.getElementById("form_quantity").value = quantity;
    }
</script>
@endsection