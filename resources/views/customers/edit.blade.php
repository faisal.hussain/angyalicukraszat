@extends('layouts.app')

@section('content-header')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Customer - Edit</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Customers</li>
                </ol>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
</section>
@endsection

@section('content')
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-12">
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Edit Customer ID = {{$customer->id}}</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form id="createCustomerForm" role="form" action="{{ route('customers.update', $customer->id)}}"
                    method="POST">
                    @method('PUT')
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <label for="email">Name</label>
                            <input type="name" class="form-control" name="name" placeholder="Name"
                                value="{{$customer->name}}">
                        </div>
                        <div class="form-group">
                            <label for="email">Username</label>
                            <input type="username" class="form-control" name="username" placeholder="Username"
                                value="{{$customer->username}}">
                        </div>
                        <div class="form-group">
                            <label>Summary Ranking</label>
                            <input type="number" class="form-control" name="ranking"
                                value="{{$customer->summary_ranking}}" min="1">
                        </div>
                        <div class="form-group">
                            <label for="email">Email address</label>
                            <input type="email" class="form-control" name="email" placeholder="Enter email"
                                value="{{$customer->email}}">
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" name="password"
                                placeholder="Enter new password or leave blank to keep existing">
                        </div>

                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="isAdminCheck" @if($customer->isAdmin)
                            checked
                            @endif >
                            <label class="form-check-label" for="adminCheck">Is Admin?</label>
                        </div>
                    </div>
                    <!-- /.card-body -->

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
            {{-- card --}}

        </div>
    </div>
</section>

@endsection

@section('customjs')

@endsection