@extends('layouts.app')

@section('customcss')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('AdminLTE/plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
@endsection

@section('content-header')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Customers</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Customers</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
@endsection
<!-- Content Header (Page header) -->

@section('content')
<section class="content">
    <div class="row">
        <div class="col-12">
            <div class="card">

                <div class="card-header">
                    <h3 class="card-title">DataTable</h3>

                    <div class="card-tools">
                        <!-- Buttons, labels, and many other things can be placed here! -->
                        <!-- Here is a label for example -->
                        <a href="{{route('customers.create')}}" type="button" class="btn btn-tool">
                            <i class="fas fa-plus-circle"></i>
                        </a>
                    </div>

                </div>

                <div class="card-body">
                    <div id="customersDataTable" class="dataTables_wrapper dt-bootstrap4">
                        <div class="row">
                            <div class="col-sm-12 col-md-6"></div>
                            <div class="col-sm-12 col-md-6"></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <table class="table table-bordered table-hover dataTable table-sm" role="grid"
                                    id="customersTable">
                                    <thead>
                                        <tr>
                                            <th class="sorting" tabindex="0" width="10%">Summary Ranking</th>
                                            <th class="sorting_asc" tabindex="0" rowspan="1" colspan="1">Name</th>
                                            <th class="sorting" tabindex="0" rowspan="1" colspan="1">Username</th>
                                            <th class="sorting" tabindex="0" rowspan="1" colspan="1">Email</th>
                                            <th colspan="1">Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($customers as $customer)
                                        <tr role="row">
                                            <td class="sorting_1">{{$customer->summary_ranking}}</td>
                                            <td>{{$customer->name}}</td>
                                            <td>{{$customer->username}}</td>
                                            <td>{{$customer->email}}</td>
                                            <td>
                                                <form action="{{ route('customers.destroy', $customer->id)}}"
                                                    method="POST">

                                                    {{-- Prevent non-admins from editing if they somehow reach this page --}}
                                                    @can('is-Admin')
                                                    <a href="{{ route('customers.edit', $customer->id)}}"
                                                        class="btn btn-primary btn-sm">
                                                        <i class="fas fa-edit"></i>
                                                    </a>
                                                    @endcan

                                                    @cannot('isSameUser', $customer->id)

                                                    <button class="btn btn-danger btn-sm" type="submit">
                                                        <i class="far fa-minus-square"></i>
                                                    </button>

                                                    @endcannot
                                                    @method('delete')
                                                    @csrf
                                                </form>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    {{-- customersDataTable --}}

                </div>
                {{-- CardBody --}}

            </div>
            {{-- card --}}

        </div>
    </div>
</section>
@endsection

@section('customjs')

<script src="{{ asset('AdminLTE/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src=" {{ asset('AdminLTE/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>

<script>
    $(document).ready(function() {
        $('#customersTable').DataTable();
    });
</script>

@endsection