@extends('layouts.app')

@section('customcss')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('AdminLTE/plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
@endsection

@section('content-header')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Special Orders</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Special Orders</li>
                </ol>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
</section>
@endsection

@section('content')
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-12">
            <div class="card">

                <div class="card-header">
                    <h3 class="card-title">DataTable</h3>

                    <div class="card-tools">
                        <!-- Buttons, labels, and many other things can be placed here! -->
                        <!-- Here is a label for example -->
                        <a href="{{route('specialorders.create')}}" type="button" class="btn btn-tool">
                            <i class="fas fa-plus-circle"></i>
                        </a>
                    </div>

                </div>

                <div class="card-body">
                    <div id="specialOrdersDataTable" class="dataTables_wrapper dt-bootstrap4">
                        <div class="row">
                            <div class="col-sm-12 col-md-6"></div>
                            <div class="col-sm-12 col-md-6"></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <table class="table table-bordered table-hover dataTable table-sm" role="grid"
                                    id="specialOrdersTable">
                                    <thead>
                                        <tr>
                                            <th class="sorting_asc" tabindex="0" rowspan="1" colspan="1">Date</th>
                                            <th class="sorting" tabindex="0" rowspan="1" colspan="1">Customer</th>
                                            <th class="sorting" tabindex="0" rowspan="1" colspan="1">Product</th>
                                            <th colspan="1">Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($specialorders as $specialorder)
                                        <tr role="row">
                                            <td>{{$specialorder->order_date->format('D d/m/Y')}}</td>
                                            <td>{{$specialorder->customer->name}}</td>
                                            <td>{{$specialorder->product->name}}</td>
                                            <td>
                                                <a href="{{ route('specialorders.edit', $specialorder->id)}}"
                                                    class="btn btn-primary btn-sm">
                                                    <i class="fas fa-edit"></i>
                                                </a>

                                                <button class="btn btn-danger btn-sm" type="submit" form="deleteForm"
                                                    formaction="{{ route('specialorders.destroy', $specialorder->id)}}">
                                                    <i class="far fa-minus-square"></i>
                                                </button>

                                                <a href="{{ route('specialorders.show', $specialorder->id)}}"
                                                    class="btn btn-primary btn-sm">
                                                    <i class="fas fa-download"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    {{-- ordersDataTable --}}

                </div>
                {{-- CardBody --}}

            </div>
            {{-- card --}}

        </div>
    </div>
</section>

@endsection

@section('customjs')

<script src="{{ asset('AdminLTE/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src=" {{ asset('AdminLTE/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>

<script>
    $(document).ready(function() {
        $('#specialOrdersTable').DataTable();
    });
</script>

@endsection