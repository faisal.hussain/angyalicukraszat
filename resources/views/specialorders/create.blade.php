@extends('layouts.app')

@section('content-header')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Special Order - Create</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Special Orders</li>
                </ol>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
</section>
@endsection

@section('content')
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-12">
            <div class="card">

                <div class="card-header">
                    <h3 class="card-title">New Order</h3>

                    <div class="card-tools">
                        <!-- Buttons, labels, and many other things can be placed here! -->
                        <!-- Here is a label for example -->
                    </div>

                </div>

                <div class="card-body">

                    <form id="createOrderForm" action="{{action('SpecialOrdersController@store')}}" method="POST">
                        @csrf

                        <div class="row">
                            {{-- Column A --}}
                            {{-- Customer Name and DatePicker --}}
                            <div class="col-sm-6">
                                @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif
                                <label for="inlineFormInput">Customer:</label>
                                <div class="form-group">
                                    @can('is-Admin')
                                    <select class="form-control customers-single">
                                        @foreach ($customers as $customer)
                                        <option value={{$customer->id}}>{{$customer->name}}</option>
                                        @endforeach
                                        <option value={{ Auth::user()->id }} selected>{{ Auth::user()->name }}</option>
                                    </select>
                                    @else
                                    <input type="text" class="form-control mb-2" name="name"
                                        placeholder="{{ Auth::user()->name }}" disabled="disabled">
                                    <input type="hidden" class="form-control mb-2" name="form_customer"
                                        value="{{ Auth::user()->id }}">
                                    @endcan
                                </div>

                                <label>Date range:</label>

                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="far fa-calendar-alt"></i>
                                            </span>
                                        </div>
                                        <input type="text" class="form-control float-right" id="orderDate"
                                            name="form_order_date">
                                    </div>
                                    <!-- /.input group -->
                                </div>


                                <label>Extra Info:</label>
                                <div class="mb-3">
                                    <textarea class="textarea" placeholder="Place some text here"
                                        name="form_special_product"
                                        style="width: 100%; height: 80px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                                </div>
                            </div>

                            {{-- Column B --}}
                            {{-- Product and Picture --}}
                            <div class="col-sm-6">
                                <label for="form_product"> Product:</label>
                                <div class="form-group">
                                    <select class="form-control cakes-with-pictures-single">
                                        @foreach ($products as $product)
                                        <option value={{$product->thumbnail}}>{{$product->name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                {{-- <div class="form-group">
                                    <img class="img-thumbnail" src="../storage/product_thumbnails/barackos-turo.png"
                                        width="200" height="200">
                                </div> --}}

                                <div class="form-group">
                                    <label for="thumbnailFile">Upload thumbnail</label>
                                    <div class="input-group">
                                        <!-- https://github.com/Johann-S/bs-custom-file-input -->
                                        <div class="custom-file">
                                            <label class="custom-file-label" for="thumbnailFile">Choose file</label>
                                            <input type="file" class="form-control" id="thumbnailFile"
                                                name="thumbnailFile" accept=".jpg,.jpeg">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                {{-- CardBody --}}

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary" form="createOrderForm">Submit</button>
                </div>

            </div>
            {{-- card --}}

        </div>
    </div>
</section>

@endsection

@section('customjs')
<script>
    function formatState(state) {
        if (!state.id) {
            return state.text;
        }
        var $state = $(
            '<span><img src="../storage/' + state.id + '" class="img-flag" width="50" height="50"/> ' + state.text + '</span>'
        );
        return $state;
    };

    $(document).ready(function() {
        $('.cakes-with-pictures-single').select2({
            templateResult: formatState,
            debug: 'true'
        });

        $('.customers-single').select2({
            debug: 'true'
        });

        //Date range picker with time picker
        $('#orderDate').daterangepicker({
            timePicker: false, //<==MAKE THE CHANGE HERE
            singleDatePicker: true, //<==MAKE THE CHANGE HERE
        });

        $('.cakes-with-pictures-single').on('select2:select', function (e) {
            $('.img-thumbnail').attr("src", "../storage/" + e.params.data.id);
        });
    });
</script>
@endsection