<?php

namespace App;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class DeployTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testCommandExecution()
    {
        $user = factory(User::class)->make([
            'isAdmin' => '0',
        ]);

        $this->actingAs($user);
        $response = $this->post('/deploy', ['command' => 'echo 123']);
        $response->assertStatus(401);

        $user = factory(User::class)->make([
            'isAdmin' => '1',
        ]);

        $this->actingAs($user)
            ->post('/deploy', ['command' => 'echo 123'])
            ->assertSee('123');
    }
}
