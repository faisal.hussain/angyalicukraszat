<?php

namespace App;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Schema;

class UserTest extends TestCase
{
    use RefreshDatabase;

    public function testUsersTableHasExpectedColumns()
    {
        $this->assertTrue(
            Schema::hasColumns('users', [
                'id', 'name', 'email', 'email_verified_at', 'password',
                'remember_token', 'created_at', 'updated_at', 'isAdmin', 'username'
            ]),
            1
        );
    }

    public function testUserHasManyOrders()
    {
        $user    = factory(User::class)->create();
        $order    = factory(Order::class)->create(['customer_id' => $user->id]);
        $order2    = factory(Order::class)->create(['customer_id' => $user->id]);

        $this->assertEquals(2, $user->orders()->count());
        $this->assertTrue($user->orders()->get()->contains($order2));
        $this->assertTrue($user->orders()->get()->contains($order));
        $this->assertInstanceOf('Illuminate\Database\Eloquent\Collection', $user->orders()->get());
    }
}
