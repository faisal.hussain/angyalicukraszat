<?php

namespace App;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Schema;

class OrderTest extends TestCase
{
    use RefreshDatabase;

    public function testOrdersTableHasExpectedColumns()
    {
        $this->assertTrue(
            Schema::hasColumns('orders', [
            'id','customer_id', 'order_date', 'special_orders', 'created_at', 'updated_at'
            ]),
            1
        );
    }
    
    public function testOrderHasOneUser()
    {
        $order = factory(Order::class)->create();
        $this->assertEquals(1, $order->customer()->count());
        $this->assertInstanceOf(User::class, $order->customer()->first());
        //$this->assertInstanceOf(User::class, $order->customer()); Does not work
    }
}
