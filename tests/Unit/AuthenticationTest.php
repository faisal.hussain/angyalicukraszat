<?php

namespace App;

use Tests\TestCase;

class AuthenticationTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testNoUser()
    {
        $except = array('login', 'register', 'password/reset', 'password/reset/{token}', 'password/confirm');
        $routeCollection = \Illuminate\Support\Facades\Route::getRoutes()->getRoutesByMethod()['GET'];
        foreach ($routeCollection as $value) {
            if (in_array($value->uri, $except) === false) {
                $response = $this->get($value->uri);
                $response->assertRedirect('/login');
            }
        }
    }
/*
    public function testCommonUser()
    {
    }

    public function testSuperUser()
    {

    }
    */
}
