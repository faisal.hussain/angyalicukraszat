<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            [
                'name' => 'Barackos túró',
                'weight' => 1.0, 'energy' => 1.0, 'slice_numbers' => '', 'price' => 589, 'created_at' => Carbon::now(),
                'thumbnail' => 'product_thumbnails/barackos-turo.png',
            ],
            [
                'name' => 'Brazil diós',
                'weight' => 1.0, 'energy' => 1.0, 'slice_numbers' => '', 'price' => 589, 'created_at' => Carbon::now(),
                'thumbnail' => 'product_thumbnails/brazil-dios.jpg',
            ],
            [
                'name' => 'Caffè latte',
                'weight' => 1.0, 'energy' => 1.0, 'slice_numbers' => '', 'price' => 589, 'created_at' => Carbon::now(),
                'thumbnail' => 'product_thumbnails/caffe-latte.png',
            ],
            [
                'name' => 'Csokoládétorta',
                'weight' => 1.0, 'energy' => 1.0, 'slice_numbers' => '', 'price' => 589, 'created_at' => Carbon::now(),
                'thumbnail' => 'product_thumbnails/csokolade.jpg',
            ],
            [
                'name' => 'Duplacsokoládé',
                'weight' => 1.0, 'energy' => 1.0, 'slice_numbers' => '', 'price' => 589.0, 'created_at' => Carbon::now(),
                'thumbnail' => 'product_thumbnails/duplacsokolade.jpg',
            ],
            [
                'name' => 'Erdei csokis',
                'weight' => 1.0, 'energy' => 1.0, 'slice_numbers' => '', 'price' => 589.0, 'created_at' => Carbon::now(),
                'thumbnail' => 'product_thumbnails/erdei-csokis.png',
            ],
            [
                'name' => 'Feketeerdő',
                'weight' => 1.0, 'energy' => 1.0, 'slice_numbers' => '', 'price' => 589.0, 'created_at' => Carbon::now(),
                'thumbnail' => 'product_thumbnails/fekete-erdo.jpg',
            ],
            [
                'name' => 'Franciakrémes revolúció',
                'weight' => 1.0, 'energy' => 1.0, 'slice_numbers' => '', 'price' => 589, 'created_at' => Carbon::now(),
                'thumbnail' => 'product_thumbnails/franciakremes-revolucio.jpg',
            ],
            [
                'name' => 'Hamis mézes',
                'weight' => 1.0, 'energy' => 1.0, 'slice_numbers' => '', 'price' => 589.0, 'created_at' => Carbon::now(),
                'thumbnail' => 'product_thumbnails/barackos-turo.png',
            ],
            [
                'name' => 'Ischler - nagy',
                'weight' => 1.0, 'energy' => 1.0, 'slice_numbers' => '', 'price' => 419.0, 'created_at' => Carbon::now(),
                'thumbnail' => 'product_thumbnails/ischler.jpg',
            ],
            [
                'name' => 'Joghurtos eper',
                'weight' => 1.0, 'energy' => 1.0, 'slice_numbers' => '', 'price' => 589.0, 'created_at' => Carbon::now(),
                'thumbnail' => 'product_thumbnails/joghurtos-eper.jpg',
            ],
            [
                'name' => 'Kisdobos',
                'weight' => 1.0, 'energy' => 1.0, 'slice_numbers' => '', 'price' => 589.0, 'created_at' => Carbon::now(),
                'thumbnail' => 'product_thumbnails/kisdobos.jpg',
            ],
            [
                'name' => 'Kókuszgolyó',
                'weight' => 1.0, 'energy' => 1.0, 'slice_numbers' => '', 'price' => 345.0, 'created_at' => Carbon::now(),
                'thumbnail' => 'product_thumbnails/kokuszgolyo.jpg',
            ],
            [
                'name' => 'Kókusztekercs',
                'weight' => 1.0, 'energy' => 1.0, 'slice_numbers' => '', 'price' => 345.0, 'created_at' => Carbon::now(),
                'thumbnail' => 'product_thumbnails/kokusztekercs.jpg',
            ],
            [
                'name' => 'Mákos guba',
                'weight' => 1.0, 'energy' => 1.0, 'slice_numbers' => '', 'price' => 589.0, 'created_at' => Carbon::now(),
                'thumbnail' => 'product_thumbnails/makos-guba.jpg',
            ],
            [
                'name' => 'Málnavarázs',
                'weight' => 1.0, 'energy' => 1.0, 'slice_numbers' => '', 'price' => 589.0, 'created_at' => Carbon::now(),
                'thumbnail' => 'product_thumbnails/malnavarazs.jpg',
            ],
            [
                'name' => 'Oreo-szelet',
                'weight' => 1.0, 'energy' => 1.0, 'slice_numbers' => '', 'price' => 589.0, 'created_at' => Carbon::now(),
                'thumbnail' => 'product_thumbnails/oreo-szelet.jpg',
            ],
            [
                'name' => 'Paleo bounty',
                'weight' => 1.0, 'energy' => 1.0, 'slice_numbers' => '', 'price' => 750.0, 'created_at' => Carbon::now(),
                'thumbnail' => 'product_thumbnails/paleo-bounty.jpg',
            ],
            [
                'name' => 'Paleo csokis málna',
                'weight' => 1.0, 'energy' => 1.0, 'slice_numbers' => '', 'price' => 750.0, 'created_at' => Carbon::now(),
                'thumbnail' => 'product_thumbnails/paleo-csokis-malna.jpg',
            ],
            [
                'name' => 'Paleo diós karamella',
                'weight' => 1.0, 'energy' => 1.0, 'slice_numbers' => '', 'price' => 750.0, 'created_at' => Carbon::now(),
                'thumbnail' => 'product_thumbnails/paleo-dios-karamella.jpg',
            ],
            [
                'name' => 'Paleo Sacher',
                'weight' => 1.0, 'energy' => 1.0, 'slice_numbers' => '', 'price' => 750.0, 'created_at' => Carbon::now(),
                'thumbnail' => 'product_thumbnails/paleo-sacher.jpg',
            ],
            [
                'name' => 'Pite - almás',
                'weight' => 1.0, 'energy' => 1.0, 'slice_numbers' => '', 'price' => 419.0, 'created_at' => Carbon::now(),
                'thumbnail' => 'product_thumbnails/pite-almas.jpg',
            ],
            [
                'name' => 'Pite - meggyes',
                'weight' => 1.0, 'energy' => 1.0, 'slice_numbers' => '', 'price' => 419.0, 'created_at' => Carbon::now(),
                'thumbnail' => 'product_thumbnails/pite-meggyes.jpg',
            ],
            [
                'name' => 'Pozsonyi diós patkó',
                'weight' => 1.0, 'energy' => 1.0, 'slice_numbers' => '', 'price' => 525.0, 'created_at' => Carbon::now(),
                'thumbnail' => 'product_thumbnails/pozsonyi-dios-patko.png',
            ],
            [
                'name' => 'Pozsonyi mákos patkó',
                'weight' => 1.0, 'energy' => 1.0, 'slice_numbers' => '', 'price' => 525.0, 'created_at' => Carbon::now(),
                'thumbnail' => 'product_thumbnails/pozsonyi-makos-patko.png',
            ],
            [
                'name' => 'Royal bomba',
                'weight' => 1.0, 'energy' => 1.0, 'slice_numbers' => '', 'price' => 345.0, 'created_at' => Carbon::now(),
                'thumbnail' => 'product_thumbnails/royal-bomb.jpg',
            ],
            [
                'name' => 'Somlói galuska',
                'weight' => 1.0, 'energy' => 1.0, 'slice_numbers' => '', 'price' => 645.0, 'created_at' => Carbon::now(),
                'thumbnail' => 'product_thumbnails/somloi-galuska.jpg',
            ],
            [
                'name' => 'Tiramisu',
                'weight' => 1.0, 'energy' => 1.0, 'slice_numbers' => '', 'price' => 589.0, 'created_at' => Carbon::now(),
                'thumbnail' => 'product_thumbnails/tiramisu.jpg',
            ],
            [
                'name' => 'Túró Rudolf',
                'weight' => 1.0, 'energy' => 1.0, 'slice_numbers' => '', 'price' => 589.0, 'created_at' => Carbon::now(),
                'thumbnail' => 'product_thumbnails/turo-rudolf.jpg',
            ]
        ]);
    }
}
