<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\Product;
use App\User;

class OrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ordersArray = [];
        $slicesArray = [];
        $incrementalCounter = 1;

        foreach (User::all() as $key => $value) {
            array_push($ordersArray, ['customer_id' => $value['id'], 'order_date' => Carbon::now(), 'special_orders' => 'Created by Seeder']);
            array_push($ordersArray, ['customer_id' => $value['id'], 'order_date' => Carbon::tomorrow(), 'special_orders' => 'Created by Seeder']);
            array_push($ordersArray, ['customer_id' => $value['id'], 'order_date' => Carbon::tomorrow()->addDays(1), 'special_orders' => 'Created by Seeder']);

            foreach (Product::all() as $key => $value) {
                array_push($slicesArray, ['product_id' => $value['id'], 'order_id' => $incrementalCounter, 'quantity' => rand(0, 10), 'price' => $value['price']]);
                array_push($slicesArray, ['product_id' => $value['id'], 'order_id' => $incrementalCounter+1, 'quantity' => rand(0, 10), 'price' => $value['price']]);
                array_push($slicesArray, ['product_id' => $value['id'], 'order_id' => $incrementalCounter+2, 'quantity' => rand(0, 10), 'price' => $value['price']]);
            }

            $incrementalCounter += 3;
        }

        DB::table('orders')->insert($ordersArray);
        DB::table('slices')->insert($slicesArray);
    }
}
