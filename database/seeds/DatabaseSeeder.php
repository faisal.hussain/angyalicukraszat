<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => 'Faisal',
                'email' => 'muscle@outlook.com',
                'username' => 'faisal',
                'isAdmin' => '1',
                'password' => bcrypt('12345678'),
                'summary_ranking' => 50
            ], [
                'name' => 'Eszti',
                'email' => '1@outlook.com',
                'username' => 'Eszti',
                'isAdmin' => '0',
                'password' => bcrypt('12345678'),
                'summary_ranking' => 1
            ], [
                'name' => 'Diet',
                'email' => 'tes1t@outlook.com',
                'username' => 'Diet',
                'isAdmin' => '0',
                'password' => bcrypt('12345678'),
                'summary_ranking' => 2
            ], [
                'name' => 'Huvi',
                'email' => 'te2st@outlook.com',
                'username' => 'Huvi',
                'isAdmin' => '0',
                'password' => bcrypt('12345678'),
                'summary_ranking' => 3
            ], [
                'name' => 'Nyugi',
                'email' => 'tes3t@outlook.com',
                'username' => 'Nyugi',
                'isAdmin' => '0',
                'password' => bcrypt('12345678'),
                'summary_ranking' => 4
            ], [
                'name' => 'Szeg',
                'email' => 'tes4t@outlook.com',
                'username' => 'Szeg',
                'isAdmin' => '0',
                'password' => bcrypt('12345678'),
                'summary_ranking' => 5
            ], [
                'name' => '16.',
                'email' => 'te5st@outlook.com',
                'username' => '16.',
                'isAdmin' => '0',
                'password' => bcrypt('12345678'),
                'summary_ranking' => 6
            ], [
                'name' => 'Zichy',
                'email' => 'te6st@outlook.com',
                'username' => 'Zichy',
                'isAdmin' => '0',
                'password' => bcrypt('12345678'),
                'summary_ranking' => 6
            ], [
                'name' => 'Hall',
                'email' => 'te7st@outlook.com',
                'username' => 'Hall',
                'isAdmin' => '0',
                'password' => bcrypt('12345678'),
                'summary_ranking' => 7
            ], [
                'name' => 'DH',
                'email' => 'tes8t@outlook.com',
                'username' => 'DH',
                'isAdmin' => '0',
                'password' => bcrypt('12345678'),
                'summary_ranking' => 8
            ], [
                'name' => 'XX',
                'email' => 'te9st@outlook.com',
                'username' => 'XX',
                'isAdmin' => '0',
                'password' => bcrypt('12345678'),
                'summary_ranking' => 9
            ], [
                'name' => 'XVII',
                'email' => '1test@outlook.com',
                'username' => 'XVII',
                'isAdmin' => '0',
                'password' => bcrypt('12345678'),
                'summary_ranking' => 10
            ], [
                'name' => 'Hope',
                'email' => '2test@outlook.com',
                'username' => 'Hope',
                'isAdmin' => '0',
                'password' => bcrypt('12345678'),
                'summary_ranking' => 11
            ], [
                'name' => 'Mea',
                'email' => '3test@outlook.com',
                'username' => 'Mea',
                'isAdmin' => '0',
                'password' => bcrypt('12345678'),
                'summary_ranking' => 12
            ], [
                'name' => 'Batta',
                'email' => '4test@outlook.com',
                'username' => 'Batta',
                'isAdmin' => '0',
                'password' => bcrypt('12345678'),
                'summary_ranking' => 13
            ], [
                'name' => 'Lech',
                'email' => '5test@outlook.com',
                'username' => 'Lech',
                'isAdmin' => '0',
                'password' => bcrypt('12345678'),
                'summary_ranking' => 14
            ], [
                'name' => 'Piri',
                'email' => '6test@outlook.com',
                'username' => 'Piri',
                'isAdmin' => '0',
                'password' => bcrypt('12345678'),
                'summary_ranking' => 15
            ], [
                'name' => 'XIII',
                'email' => '7test@outlook.com',
                'username' => 'XIII',
                'isAdmin' => '0',
                'password' => bcrypt('12345678'),
                'summary_ranking' => 16
            ], [
                'name' => 'Zsuzsi',
                'email' => '8test@outlook.com',
                'username' => 'Zsuzsi',
                'isAdmin' => '0',
                'password' => bcrypt('12345678'),
                'summary_ranking' => 17
            ], [
                'name' => 'Bali',
                'email' => '9test@outlook.com',
                'username' => 'Bali',
                'isAdmin' => '0',
                'password' => bcrypt('12345678'),
                'summary_ranking' => 18
            ], [
                'name' => 'Csoda',
                'email' => 't11est@outlook.com',
                'username' => 'Csoda',
                'isAdmin' => '0',
                'password' => bcrypt('12345678'),
                'summary_ranking' => 19
            ], [
                'name' => 'Viola',
                'email' => 't22est@outlook.com',
                'username' => 'Viola',
                'isAdmin' => '0',
                'password' => bcrypt('12345678'),
                'summary_ranking' => 20
            ], [
                'name' => 'Vecs',
                'email' => 't222est@outlook.com',
                'username' => 'Vecs',
                'isAdmin' => '0',
                'password' => bcrypt('12345678'),
                'summary_ranking' => 21
            ], [
                'name' => 'Bebe',
                'email' => 't111est@outlook.com',
                'username' => 'Bebe',
                'isAdmin' => '0',
                'password' => bcrypt('12345678'),
                'summary_ranking' => 22
            ], [
                'name' => 'Mari',
                'email' => 'tes111t@outlook.com',
                'username' => 'Mari',
                'isAdmin' => '0',
                'password' => bcrypt('12345678'),
                'summary_ranking' => 23
            ], [
                'name' => 'Mokka',
                'email' => 'te111st@outlook.com',
                'username' => 'Mokka',
                'isAdmin' => '0',
                'password' => bcrypt('12345678'),
                'summary_ranking' => 24
            ], [
                'name' => 'Kalvin',
                'email' => 'test111@outlook.com',
                'username' => 'Kalvin',
                'isAdmin' => '0',
                'password' => bcrypt('12345678'),
                'summary_ranking' => 25
            ], [
                'name' => 'Lobi',
                'email' => 'test121@outlook.com',
                'username' => 'Lobi',
                'isAdmin' => '0',
                'password' => bcrypt('12345678'),
                'summary_ranking' => 26
            ], [
                'name' => 'Baro',
                'email' => 'tes212t@outlook.com',
                'username' => 'Baro',
                'isAdmin' => '0',
                'password' => bcrypt('12345678'),
                'summary_ranking' => 27
            ]
        ]);

        $this->call(ProductsTableSeeder::class);
        $this->call(OrdersTableSeeder::class);
        $this->call(SpecialOrderSeeder::class);
    }
}
