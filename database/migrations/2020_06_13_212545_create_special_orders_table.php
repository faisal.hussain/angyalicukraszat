<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpecialOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('special_orders', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedInteger('customer_id');
            $table->date('order_date');
            $table->unsignedInteger('product_id');
            
            // Note that the TEXT data is not stored in the database server's memory, therefore, whenever you query TEXT data,
            // MySQL has to read from it from the disk, which is much slower in comparison with CHAR and VARCHAR
            $table->string('picture')->nullable();
            
            // https://stackoverflow.com/questions/4851672/one-mysql-table-with-multiple-timestamp-columns
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('special_orders');
    }
}
