<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Reference:
        // http://www.angyalicuki.hu/doksik/angyali-termekinformacios-tablazat.pdf
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            
            // E.g Kokuszterecs
            $table->string('name');

            // Szénhidrát (g)
            $table->decimal('weight',10,2)->unsigned();

            // Energia (kcal)
            $table->decimal('energy',10,2)->unsigned();

            // Tortaként elérhető szeletszámok
            $table->string('slice_numbers');

            // Price cannot be negative so setting it to unsigned. 10 digits for place before decimal and 2 digits for after deicmal point.
            $table->decimal('price',10,2)->unsigned();

            // Note that the TEXT data is not stored in the database server's memory, therefore, whenever you query TEXT data,
            // MySQL has to read from it from the disk, which is much slower in comparison with CHAR and VARCHAR
            $table->string('thumbnail')->nullable();

            // https://stackoverflow.com/questions/4851672/one-mysql-table-with-multiple-timestamp-columns
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
