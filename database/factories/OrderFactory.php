<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Order;
use Faker\Generator as Faker;
use Carbon\Carbon;

$factory->define(Order::class, function (Faker $faker) {
    return [
        //'id' => Auto increment
        'customer_id' => factory(App\User::class),
        'order_date' => $faker->date($format = 'Y-m-d', $max = 'now'),
        'special_orders' => $faker->paragraph,
        'created_at' => $faker->date($format = 'Y-m-d H:i:s', $max = 'now'),
        'updated_at' => $faker->date($format = 'Y-m-d H:i:s', $max = 'now')
    ];
});
